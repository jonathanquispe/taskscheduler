import Vue from 'vue'

import App from './App.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import {ClientTable} from 'vue-tables-2';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

let options = {};
let useVuex = false;
let theme = "bootstrap4";
let template = "default";
Vue.use(ClientTable, options, useVuex, theme, template);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

Vue.config.productionTip = false
new Vue({
  render: h => h(App)
}).$mount('#app')

