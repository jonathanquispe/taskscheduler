
class xCron {
    constructor() {
        this.minutes = null;
        this.hours = null;
        this.days = null;
        this.months = null;
        this.dates = null;

        this.settings = {};
    }
    //Periodicity
    //Starting time, Ending Time is a format in 0-23
    //RepeatEvery {only weeks}
    //RepeatOn [0,1,2,3,4,5,6]
    toExpression(periodicity, repeatEvery, repeatOn) {
        let parse = periodicity.split(":");
        this.minutes = `*/${parseInt(parse[1])}`;
        if(parseInt(parse[0]) == 0){
            this.hours = `*`;
        }else{
            this.hours = `*/${parseInt(parse[0])}`;
        }
     
        if (parseInt(repeatEvery / 4) == 0) {
            this.days = `*/${repeatEvery % 4}`;
            this.months = `*`;
        } else {
            this.days = repeatEvery % 4 == 0? '*':`*/${repeatEvery % 4}`;
            this.months = `*/${parseInt(repeatEvery / 4)}`;
        }
        this.dates = `${repeatOn.join(",")}`;
        return `${this.minutes} ${this.hours} ${this.days} ${this.months} ${this.dates}`
    }
    //Settings
    // Expression is * * * * *
    toSettings(expression) {
        let parse = expression.split(/\s+/);
        this.minutes = parse[0];
        this.hours = parse[1];
        this.days = parse[2];
        this.months = parse[3];
        this.dates = parse[4];
        if (this.hours == "*" && this.minutes == "*") {
            this.settings.periodicity = "0:01";
        } else {            
            if(this.hours.split("/").length > 1){
                this.settings.periodicity = `${this.hours.split("/")[1]}:${this.minutes.split("/")[1]}`;
            }
        }

        if (this.months == "*" && this.days == "*") {
            this.settings.repeatEvery = 1;
        } else {
            if(this.months.split("/").length > 1 && this.days.split("/").length){
                this.settings.repeatEvery = parseInt(this.months.split("/")[1]) * 4 + parseInt(this.days.split("/")[1]);
            }else{
                this.settings.repeatEvery = 1;
            }
        }

        if (this.dates == "*") {
            this.settings.repeatOn= ['0','1','2','3','4','5','6'];
        } else {
            this.settings.repeatOn = this.dates.split(",");
        }

        return this.settings;
    }
}

exports.xCron = xCron;