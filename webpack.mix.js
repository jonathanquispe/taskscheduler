const mix = require('laravel-mix');

mix.setPublicPath('../public_html')
   .copy('dist/', '../public_html')
   .copy('public/index.html', '../public_html')
   .version();